# Projet Positionnement et Flex

## Exo first-flex [html](first-flex.html)/[css](css/first-flex.css)
1. Créer un fichier first-flex.html et lui lier un fichier first-flex.css
2. Dans le fichier HTML, créer un élément main contenant 3 sections avec du texte dedans
3. Dans le fichier css, faire en sorte d'encadrer les ptites sections (genre avec une bordure), puis les afficher en ligne en utilisant le flex 


## Exo Flex Site Trois colonnes [html](three-columns.html)/[css](css/three-columns.css)
Reproduire ce wireframe en HTML/CSS en utilisant les flex box :

![wireframe 3 colonnes](wireframes/flex.png)